import data.Location

interface AirQualityContract {

    interface View {
        fun showCities(cities: List<City>)
        fun showMeasurements(locations: List<Location>)
        fun showLoader()
        fun hideLoader()
        fun onCityClickListener(city: City)
    }

    interface Presenter {
        fun attach(view: View)
        fun loadCities()
        fun loadMeasurements(city: City)
        fun sortCitiesAscending()
        fun sortCitiesDescending()
    }
}