import data.Location
import org.w3c.xhr.XMLHttpRequest

class PresenterImpl : AirQualityContract.Presenter {

    lateinit var view: AirQualityContract.View
    lateinit var citiesList: MutableList<City>

    override fun attach(view: AirQualityContract.View) {
        this.view = view
    }

    override fun loadCities() {
        view.showLoader()
        getAsync(API_CITIES_POLAND) {
            val jsonResponse = JSON.parse<JSONResponse<City>>(it)
            val cities = jsonResponse.results
            citiesList = cities.toMutableList()
            citiesList.sortBy { city -> city.city }
            view.hideLoader()
            view.showCities(citiesList)
        }
    }

    override fun loadMeasurements(city: City) {
        view.showLoader()
        getAsync(API_MEASUREMENTS + city.city) {
            val jsonResponse = JSON.parse<JSONResponse<Location>>(it)
            val locations = jsonResponse.results.toList()
            view.hideLoader()
            view.showMeasurements(locations)
        }
    }

    override fun sortCitiesAscending() {
        view.showLoader()
        citiesList.sortBy { city -> city.city }
        view.showCities(citiesList)
        view.hideLoader()
    }

    override fun sortCitiesDescending() {
        view.showLoader()
        citiesList.sortByDescending { city -> city.city }
        view.showCities(citiesList)
        view.hideLoader()
    }

    private fun getAsync(url: String, callback: (String) -> Unit) {
        val xmlHttp = XMLHttpRequest()
        xmlHttp.open("GET", url)

        xmlHttp.onload = {
            if (xmlHttp.readyState == 4.toShort() && xmlHttp.status == 200.toShort()) {
                callback.invoke(xmlHttp.responseText)
            }
        }
        xmlHttp.send()
    }
}
