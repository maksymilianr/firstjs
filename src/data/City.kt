data class City(val city: String,
                val country: String,
                val locations: String,
                val count: String)

data class JSONResponse<T>(val results: Array<T>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is JSONResponse<*>) return false

        if (!results.contentEquals(other.results)) return false

        return true
    }

    override fun hashCode(): Int {
        return results.contentHashCode()
    }
}