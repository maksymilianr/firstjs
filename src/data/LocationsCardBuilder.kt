package data

import org.w3c.dom.Element
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLElement
import kotlin.browser.document
import kotlin.dom.addClass

class LocationsCardBuilder {


    fun buildLocation(location: Location): HTMLElement {
        val containerElement = document.createElement("div") as HTMLDivElement
        val locationName = document.createElement("div") as HTMLDivElement
        val cityName = document.createElement("div") as HTMLDivElement

        bind(location = location, locationName = locationName, cityName = cityName)

        applyStyle(containerElement, locationName, cityName)
        containerElement.appendChildForAll(locationName, cityName)

        location.measurements.forEach {
            val measurementBuilder = MeasurementBuilder()
            val measurementElement = measurementBuilder.buildMeasurement(it)
            containerElement.appendChild(measurementElement)
        }

        return containerElement
    }

    private fun bind(location: Location, locationName: HTMLDivElement, cityName: HTMLDivElement) {
        locationName.innerHTML = location.location
        cityName.innerHTML = location.city
    }

    private fun applyStyle(containerElement: HTMLDivElement, locationName: HTMLDivElement, cityName: HTMLDivElement) {
        containerElement.addClass("card", "location-card")
    }

    class MeasurementBuilder {
        fun buildMeasurement(measurement: Measurement): HTMLElement {
            val containerElement = document.createElement("div") as HTMLDivElement
            val parameter = document.createElement("div") as HTMLDivElement
            val valueWithUnit = document.createElement("div") as HTMLDivElement
            val lastUpdated = document.createElement("div") as HTMLDivElement

            bind(measurement, parameter, valueWithUnit, lastUpdated)

            parameter.style.backgroundColor = chooseColor(calculatePercentValue(measurement))
            parameter.addClass("parameter")

            containerElement.addClass("measurement")
            containerElement.appendChildForAll(parameter, valueWithUnit, lastUpdated)
            return containerElement
        }

        private fun bind(measurement: Measurement, parameter: HTMLDivElement, value: HTMLDivElement, lastUpdated: HTMLDivElement) {
            parameter.innerHTML = measurement.parameter + " : " + calculatePercentValue(measurement) + "%"
            value.innerHTML = measurement.value + " " + measurement.unit
            lastUpdated.innerHTML = measurement.lastUpdated.convertDate()
        }
    }
}

private fun calculatePercentValue(measurement: Measurement): Int {
    var maximumValue = 0
    when (measurement.parameter) {
        "bc" -> maximumValue = 5
        "no2" -> maximumValue = 200
        "so2" -> maximumValue = 125
        "pm10" -> maximumValue = 50
        "co" -> maximumValue = 10000
        "pm25" -> maximumValue = 25
        "o3" -> maximumValue = 120
    }
    return (measurement.value.toDouble() * 100 / maximumValue).toInt()
}

private fun chooseColor(percentValue: Int): String {
    if (percentValue < 25) return "#A3F1A2"
    if (percentValue < 50) return "#BCC283"
    if (percentValue < 75) return "#D49364"
    return "#EC6546"
}

private fun String.convertDate(): String = this.replace('T', ' ').replace(":00.000Z", "")

private fun Element.appendChildForAll(vararg elements: Element) {
    elements.forEach { this.appendChild(it) }
}