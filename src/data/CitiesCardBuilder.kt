import org.w3c.dom.Element
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLElement
import kotlin.browser.document
import kotlin.dom.addClass

class CitiesCardBuilder(private val listener: AirQualityContract.View) {

    fun buildCities(city: City): HTMLElement {
        val containerElement = document.createElement("div") as HTMLDivElement
        val name = document.createElement("div") as HTMLDivElement

        bind(city = city, name = name)

        applyStyle(containerElement, name = name)

        containerElement.appendChildForAll(name)
        containerElement.addEventListener("click", { listener.onCityClickListener(city) })

        return containerElement
    }

    private fun Element.appendChildForAll(vararg elements: Element) {
        elements.forEach { this.appendChild(it) }
    }

    private fun bind(city: City, name: HTMLDivElement) {
        name.innerHTML = city.city
    }

    private fun applyStyle(containerElement: HTMLDivElement, name: HTMLDivElement) {
        containerElement.addClass("card", "card-show")
        name.addClass("city-text")
    }
}