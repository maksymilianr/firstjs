package data

data class Location(val location: String,
                    val city: String, val measurements: Array<Measurement>)

data class Measurement(val parameter: String,
                       val value: String,
                       val lastUpdated: String,
                       val unit: String)
