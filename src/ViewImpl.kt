import data.Location
import data.LocationsCardBuilder
import org.w3c.dom.HTMLDivElement
import kotlin.browser.document
import kotlin.dom.clear

class ViewImpl(val presenter: AirQualityContract.Presenter) : AirQualityContract.View {

    init {
        presenter.attach(this)

        (document.getElementById("sortAscending") as HTMLDivElement)
                .addEventListener("click", { presenter.sortCitiesAscending() })
        (document.getElementById("sortDescending") as HTMLDivElement)
                .addEventListener("click", { presenter.sortCitiesDescending() })
    }

    private val loader = document.getElementById("loader") as HTMLDivElement
    private val citiesContent = document.getElementById("cities") as HTMLDivElement
    private val locationsContent = document.getElementById("locations") as HTMLDivElement
    private val citiesCardBuilder = CitiesCardBuilder(this)
    private val locationsCardBuilder = LocationsCardBuilder()

    override fun showCities(cities: List<City>) {
        citiesContent.clear()
        cities.forEach { city ->
            val card = citiesCardBuilder.buildCities(city)
            citiesContent.appendChild(card)
        }
    }

    override fun showMeasurements(locations: List<Location>) {
        locationsContent.clear()
        locations.forEach { location ->
            val card = locationsCardBuilder.buildLocation(location)
            locationsContent.appendChild(card)
        }
    }

    override fun showLoader() {
        loader.style.display = "inline"
        loader.style.visibility = "visible"
    }

    override fun hideLoader() {
        loader.style.visibility = "hidden"
        loader.style.display = "none"
    }

    override fun onCityClickListener(city: City) {
        presenter.loadMeasurements(city)
    }
}