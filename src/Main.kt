fun main() {
    println("Hello world!")
    val presenter = PresenterImpl()
    val view = ViewImpl(presenter)

    presenter.loadCities()

}

val API_CITIES_POLAND = "https://api.openaq.org/v1/cities?country=PL&limit=300"
val API_MEASUREMENTS = "https://api.openaq.org/v1/latest?city="     //+ cityName